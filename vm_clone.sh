#!/bin/sh

DATASTORE="/vmfs/volumes/datastore1"

if [ "x" == "x$2" ]; then
	echo "usage $0 <vm src name> <new name>"
	exit 1
fi

DSTNAME=$2
SRCNAME=$1

echo Cloning $SRCNAME to $DSTNAME

# create storage
mkdir $DATASTORE/$DSTNAME
vmkfstools -i $DATASTORE/$SRCNAME/$SRCNAME.vmdk $DATASTORE/$DSTNAME/$DSTNAME.vmdk

# copy vmx file
cp $DATASTORE/$SRCNAME/$SRCNAME.vmx $DATASTORE/$DSTNAME/$DSTNAME.vmx

cd $DATASTORE/$DSTNAME
SED_CMD_1="sed -i.bak 's/displayName = \"$SRCNAME\"/displayName = \"$DSTNAME\"/' $DSTNAME.vmx"
SED_CMD_2="sed -i 's/scsi0:0.fileName = \"$SRCNAME.vmdk\"/scsi0:0.fileName = \"$DSTNAME.vmdk\"/' $DSTNAME.vmx"
echo $SED_CMD_1
eval "$SED_CMD_1"
echo $SED_CMD_2
eval "$SED_CMD_2"


# register new vm
vim-cmd solo/registervm $DATASTORE/$DSTNAME/$DSTNAME.vmx
VMNUMBER=$?
echo vm nummer $VMNUMER
