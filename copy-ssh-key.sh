#!/usr/bin/env bash


usage() {
	echo "usage:"
	echo "   $0 <ssh-server> <pubkeyfile>
}

if [ "x$1" == "x" ]; then
	usage
	exit 1
fi
SERVER=$1

if [ "x$2" == "x" ]; then
	usage
	exit 1
fi
PUBFILE=$2
USER="root"


echo "creating subdir"
ssh $SERVER mkdir -p /etc/ssh/keys-$USER
echo -n "adding pubkey for user $USER: "
cat $PUBFILE
cat $PUBFILE | ssh $SERVER "cat >> /etc/ssh/keys-${USER}/authorized_keys"
